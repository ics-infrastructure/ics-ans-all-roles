# README #

### What is this repository for? ###

* This git contains git submodule data to load up all ESSS owned ansible roles at once.

### How do I get set up? ###

* Summary of set up
* Configuration

How to add a new project:

    $ git submodule add git@bitbucket.org:europeanspallationsource/ics-ans-role-activemq
    $ git commit -a
    $ git push origin

How to update all submodules:

    $ git submodule update --remote

How to remove a submodule:

    $ git submodule deinit <module>
    $ git rm <module>
    $ git commit

* Dependencies
* Nightly builds
* How to run tests
* Deployment instructions

### Getting started ###
===============

How to get started with all the Ansible repos at once:

```
$ git clone --recursive git@bitbucket.org:europeanspallationsource/ics-ans-all-roles
Cloning into 'ics-ans-all-roles'...
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
Submodule 'ics-ans-role-cs-studio' (git@bitbucket.org:europeanspallationsource/ics-ans-role-cs-studio) registered for path 'ics-ans-role-cs-studio'
Submodule 'ics-ans-role-devenv-base' (git@bitbucket.org:europeanspallationsource/ics-ans-role-devenv-base) registered for path 'ics-ans-role-devenv-base'
Submodule 'ics-ans-role-filebeat' (git@bitbucket.org:europeanspallationsource/ics-ans-role-filebeat) registered for path 'ics-ans-role-filebeat'
Submodule 'ics-ans-role-openxal' (git@bitbucket.org:europeanspallationsource/ics-ans-role-openxal) registered for path 'ics-ans-role-openxal'
Submodule 'ics-ans-role-oracle-jdk' (git@bitbucket.org:europeanspallationsource/ics-ans-role-oracle-jdk) registered for path 'ics-ans-role-oracle-jdk'
Submodule 'ics-ans-role-repository' (git@bitbucket.org:europeanspallationsource/ics-ans-role-repository) registered for path 'ics-ans-role-repository'
Cloning into '/home/user/ics-ans-all-roles/ics-ans-role-cs-studio'...
remote: Counting objects: 99, done.
remote: Compressing objects: 100% (76/76), done.
remote: Total 99 (delta 36), reused 0 (delta 0)
Receiving objects: 100% (99/99), 88.58 KiB | 0 bytes/s, done.
Resolving deltas: 100% (36/36), done.
Cloning into '/home/user/ics-ans-all-roles/ics-ans-role-devenv-base'...
remote: Counting objects: 235, done.
remote: Compressing objects: 100% (224/224), done.
remote: Total 235 (delta 113), reused 0 (delta 0)
Receiving objects: 100% (235/235), 630.38 KiB | 1008.00 KiB/s, done.
Resolving deltas: 100% (113/113), done.
Cloning into '/home/user/ics-ans-all-roles/ics-ans-role-filebeat'...
remote: Counting objects: 45, done.
remote: Compressing objects: 100% (31/31), done.
remote: Total 45 (delta 10), reused 0 (delta 0)
Receiving objects: 100% (45/45), 6.18 KiB | 0 bytes/s, done.
Resolving deltas: 100% (10/10), done.
Cloning into '/home/user/ics-ans-all-roles/ics-ans-role-openxal'...
remote: Counting objects: 95, done.
remote: Compressing objects: 100% (83/83), done.
remote: Total 95 (delta 34), reused 0 (delta 0)
Receiving objects: 100% (95/95), 14.61 KiB | 0 bytes/s, done.
Resolving deltas: 100% (34/34), done.
Cloning into '/home/user/ics-ans-all-roles/ics-ans-role-oracle-jdk'...
remote: Counting objects: 91, done.
remote: Compressing objects: 100% (66/66), done.
remote: Total 91 (delta 29), reused 0 (delta 0)
Receiving objects: 100% (91/91), 15.44 KiB | 0 bytes/s, done.
Resolving deltas: 100% (29/29), done.
Cloning into '/home/user/ics-ans-all-roles/ics-ans-role-repository'...
remote: Counting objects: 52, done.
remote: Compressing objects: 100% (37/37), done.
remote: Total 52 (delta 14), reused 0 (delta 0)
Receiving objects: 100% (52/52), 6.37 KiB | 0 bytes/s, done.
Resolving deltas: 100% (14/14), done.
Submodule path 'ics-ans-role-cs-studio': checked out 'b017603a0840747dd9b64c037cb6babf5a75f87d'
Submodule path 'ics-ans-role-devenv-base': checked out '4236a3b86d563083876b959c424d7b05c034f877'
Submodule path 'ics-ans-role-filebeat': checked out '6862f86cc27cb141e5fe68a8c9d83a3f06c0d2cf'
Submodule path 'ics-ans-role-openxal': checked out 'fb074208d3f254aacce3fbca573868686d158243'
Submodule path 'ics-ans-role-oracle-jdk': checked out 'ec2b381c268b1ac9911daa8d8f1b8773eec2c272'
Submodule path 'ics-ans-role-repository': checked out '04373bf419f586f1f5e122cbe936e7b7b9cd18db'
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
